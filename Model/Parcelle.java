
import java.util.*;

public class Parcelle {

    private int ligne;
    private int colone;
    private List<Personnage> persPresents;

    public Parcelle(int ligne, int colone) {
      this.ligne = ligne;
      this.colone = colone;
      this.persPresents = new ArrayList<Personnage>();
    }

    public int getLigne(){
      return this.ligne;
    }

    public int getColone(){
      return this.colone;
    }

    public List<Personnage> getPersPresents(){
      return this.persPresents;
    }

    public void enleverPers(Personnage pers){
      this.persPresents.remove(pers);
    }

    public void ajouterPers(Personnage pers){
      this.persPresents.add(pers);
    }

    public int getPosX(){
      return this.colone;
    }

    public int getPosY(){
      return this.ligne;
    }

    public String toString(){
      String res = "";
      for(int i=0; i<this.persPresents.size();i++){
        res+=persPresents.get(i);
      }
      return res;
    }

}
