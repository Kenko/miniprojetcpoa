
import java.util.*;

public class Gnome extends Personnage {

  private Parcelle parcelle;
  private Monde monde;
  private Tribu tribu;
  private String nom;

    public Gnome(Monde monde, Parcelle parcelle, Tribu tribu, String nom) {
      super(monde, parcelle, tribu);
      this.nom=nom;
    }

    public void changerEtatStress() {
        // TODO implement here
    }

    public void notification(Elf elf, int posX, int posY) {

    }

    public String toString(){
      return this.nom+" ";
    }

}
