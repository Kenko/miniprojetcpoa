
import java.util.*;

/**
 *
 */
public class Monde {

    private List<Parcelle> parcelles;
    private int nbLignes;
    private int nbColones;

    public Monde() {
      this.parcelles = new ArrayList<Parcelle>();
      this.nbLignes=0;
      this.nbColones=0;
    }

    public void init(int nbColones, int nbLignes){
      this.nbLignes=nbLignes;
      this.nbColones=nbColones;
      for (int i =0;i<nbLignes;i++){
        for (int j =0;j<nbColones;j++){
          this.parcelles.add(new Parcelle(i,j));
        }
      }
    }

    public List<Parcelle> getParcelles(){
      return this.parcelles;
    }

    public String toString(){
      String res ="";
      int cpt=1;
      for (int i=0; i<this.parcelles.size(); i++){
        res+= "ID PC: "+ i +" "+ this.parcelles.get(i).toString()+" | ";
        cpt++;
        if(cpt>this.nbColones){
          res+="\n";
          cpt=1;
        }
      }
      return res;
    }

    public void demanderDeplacement(Elf elf){
      System.out.print("deplacement de l'elf (help)") ;
      Scanner dep = new Scanner(System.in);
      String testEcrit = dep.nextLine();

      if(testEcrit.equals("haut") || testEcrit.equals("bas") || testEcrit.equals("gauche") ||testEcrit.equals("droite") || testEcrit.equals("help")){

        if(testEcrit.equals("help")){
          System.out.println("Ecrivez la direction dans laquelle vous voulez aller: haut, gauche, bas, droite");
        }
        if(testEcrit.equals("haut")){
          Parcelle curParcelle;
          curParcelle = elf.getParcelle();
          if(curParcelle.getPosY()==0){
            System.out.println("Vous ne pouvez pas faire ça");
            demanderDeplacement(elf);
          }
          else{
            elf.setPosition(curParcelle.getPosX(), curParcelle.getPosY()-1);
          }
        }


        if(testEcrit.equals("bas")){
          Parcelle curParcelle;
          curParcelle = elf.getParcelle();
          if(curParcelle.getPosY()==this.nbLignes-1){
            System.out.println("Vous ne pouvez pas faire ça");
            demanderDeplacement(elf);
          }
          else{
            elf.setPosition(curParcelle.getPosX(), curParcelle.getPosY()+1);
          }
        }

        if(testEcrit.equals("gauche")){
          Parcelle curParcelle;
          curParcelle = elf.getParcelle();
          if(curParcelle.getPosX()==0){
            System.out.println("Vous ne pouvez pas faire ça");
            demanderDeplacement(elf);
          }
          else{
            elf.setPosition(curParcelle.getPosX()-1, curParcelle.getPosY());
          }
        }


        if(testEcrit.equals("droite")){
          Parcelle curParcelle;
          curParcelle = elf.getParcelle();
          if(curParcelle.getPosX()==this.nbColones-1){
            System.out.println("Vous ne pouvez pas faire ça");
            demanderDeplacement(elf);
          }
          else{
            elf.setPosition(curParcelle.getPosX()+1, curParcelle.getPosY());
          }
        }
        System.out.println(this);
        demanderDeplacement(elf);
      }
      else{
        System.out.println("Si vous ne savez pas quoi écrire, écrivez help") ;
        demanderDeplacement(elf);
      }
    }

    public static void main(String[] args) {
      Monde monde = new Monde();
      System.out.println("Entrée les coordonées de votre monde:");
      System.out.print("i:") ;
      Scanner r = new Scanner(System.in);
      int i = r.nextInt();
      System.out.print("j:") ;
      Scanner t = new Scanner(System.in);
      int j = t.nextInt();
      monde.init(i,j);
      Tribu tb1 = new Tribu();
      int g=0;
      for(int b=0; b<i; b++){
        for(int s=0; s<j; s++){
          g++;
          Gnome gn1= new Gnome(monde,monde.getParcelles().get(g-1),tb1,"gn"+ g);
        }

      }
      Parcelle pc1 = monde.getParcelles().get(0);
      Gnome gn1= new Gnome(monde,pc1,tb1,"gn1");
      tb1.ajouterPers(gn1);
      Elf elf1 = new Elf(monde,pc1,tb1,"elf1");

      // System.out.println(pc);
      System.out.println(monde);
      monde.demanderDeplacement(elf1);
    }

}
