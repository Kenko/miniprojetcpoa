import java.util.*;

public class Personnage {

    private Parcelle parcelle;
    private Monde monde;
    private Tribu tribu;

    public Personnage(Monde monde, Parcelle parcelle, Tribu tribu) {
      this.monde=monde;
      this.parcelle =parcelle;
      this.tribu= tribu;
      this.parcelle.ajouterPers(this);
    }

    public Parcelle getParcelle(){
      return this.parcelle;
    }

    public Monde getMonde(){
      return this.monde;
    }

    public void setPosition(int posX, int posY){
      this.parcelle.enleverPers(this);
      for(int i=0; i<this.monde.getParcelles().size();i++){
        if(this.monde.getParcelles().get(i).getPosX()==posX){
          if(this.monde.getParcelles().get(i).getPosY()==posY){
            this.parcelle=this.monde.getParcelles().get(i);
            this.monde.getParcelles().get(i).ajouterPers(this);
          }
        }
      }
    }

    public Tribu getTribu(){
      return this.tribu;
    }

}
